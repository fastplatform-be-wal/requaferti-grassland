#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

MINIMUM_BUILDOZER_VERSION=3.4.0

check_buildozer_version() {

  if ! [ -x "$(command -v buildozer)" ]; then
    echo "buildozer not in path !"
    echo "https://github.com/bazelbuild/buildtools/tree/master/buildozer"
    echo "OSX -> brew install buildozer"
    return 1
  fi

  current_buildozer_version=$(buildozer --version | head -n 1 | awk '{print $3}')
  if [[ "${MINIMUM_BUILDOZER_VERSION}" != $(echo -e "${MINIMUM_BUILDOZER_VERSION}\n${current_buildozer_version}" | sort -s -t. -k 1,1 -k 2,2n -k 3,3n | head -n1) ]]; then
    cat <<EOF
Current buildozer version: ${current_buildozer_version}.
Requires ${MINIMUM_BUILDOZER_VERSION} or greater.
EOF
    return 2
  fi
}

check_buildozer_version