import os
import sys
from typing import Dict, Union
from pathlib import Path, PurePath

from pydantic import BaseSettings


class Settings(BaseSettings):

    APP_DIR: Path = PurePath(__file__).parent

    EPSG_SRID_ETRS89: str = "4258"
    EPSG_SRID_ETRS89_LAEA: str = "3035"
    EPSG_SRID_WGS84: str = "4326"
    EPSG_SRID_BELGE_72: str = "4313"
    EPSG_SRID_BELGIAN_LAMBERT_72: str = "31370"

    OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT: str = "http://127.0.0.1:9411/api/v2/spans"
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1
    OPENTELEMETRY_SERVICE_NAME: str = "addon-requaferti-grassland-fertilization-requaferti-grassland-grassland"

    REQUAFERTI_COMPUTE_N_CACHE_MAX_SIZE: int = 10000
    REQUAFERTI_COMPUTE_N_CACHE_TTL: int = 300  # seconds
    REQUAFERTI_COMPUTE_N_URL: str = str(os.getenv("REQUAFERTI_COMPUTE_N_URL","https://www.requaferti.requasud.be/requaferti_prairie_ws"))
    REQUAFERTI_CONFIG_DICO_LIST_URL: str = str(os.getenv("REQUAFERTI_CONFIG_DICO_LIST_URL","https://www.requaferti.requasud.be/requaferti_prairie_ws/liste"))
    REQUAFERTI_CONFIG_CACHE_TTL: int = 3600
    REQUAFERTI_CONFIG_DICO_CONCURRENCY: int = 3
    REQUAFERTI_CONFIG_DICO_URL: str = str(os.getenv("REQUAFERTI_CONFIG_DICO_URL","https://www.requaferti.requasud.be/requaferti_prairie_dico_ws/dico"))
    REQUAFERTI_CONFIG_LISTE_DICOS: str =str(os.getenv("REQUAFERTI_CONFIG_LISTE_DICOS"))
    REQUAFERTI_DEFAULT_TIMEOUT: int = 10
    REQUAFERTI_PASSWORD: str
    REQUAFERTI_USERNAME: str

    REQUAFERTI_RESOURCES_IMAGES_PATH:str = str(os.getenv("REQUAFERTI_RESOURCES_IMAGES_PATH","./images/"))

    PDF_GENERATOR_ENDPOINT = str(os.getenv("PDF_GENERATOR_ENDPOINT", "http://0.0.0.0:7010/generate_pdf"))

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }

config = Settings()
