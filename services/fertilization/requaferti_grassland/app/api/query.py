from dataclasses import asdict
import os
import logging
import base64
import re

import requests
import graphene
from graphene_pydantic_updated import PydanticObjectType, PydanticInputObjectType
from graphene.types.generic import GenericScalar
from opentelemetry import trace
import shapely.ops
import shapely.geometry

from app.settings import config
from app.api.errors import GraphQLServiceError
from app.lib.requaferti_types import (
    CloverCoveragePictures,
    PrairieFournitureLeguType,
    RequafertiComputeNParams,
    RequafertiComputeNResult,
    RequafertiGetSolResultType,
    PDFGeneratorInputType,
    PDFGeneratorOutputType,
    RequafertiNResultError,
    RequafertiNResultEstimation
)
from app.lib.requaferti import requaferti_client
from app.lib.gis import Projection
from app.lib.ugb.UniteGrosBetail import ugb_client


logger = logging.getLogger(__name__)


# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)

####

class AnimalCategoryInput(graphene.InputObjectType):
    id = graphene.String()
    libelle = graphene.String(required=False)
    libelle_de = graphene.String(required=False)

class OrganicMatterTypeInput(graphene.InputObjectType):
    id = graphene.String()
    type_eff_xid = graphene.String()
    type_eff_code = graphene.String()
    type_eff_libelle = graphene.String()
    type_eff_libelle_de = graphene.String()
    grp_ani_xid = graphene.String()
    grp_ani_libelle = graphene.String()
    grp_ani_libelle_de = graphene.String()
    azote_total = graphene.String()

class SpreadingConditionInput(graphene.InputObjectType):
    id = graphene.String()
    libelle = graphene.String(required=False)
    libelle_de = graphene.String(required=False)

class PassInput(graphene.InputObjectType):
    id = graphene.ID()
    animalCategory = AnimalCategoryInput()
    organicMatterType = OrganicMatterTypeInput()
    effluentQuantity = graphene.Int()
    nitrogenContent = graphene.Float()
    applicationDate = graphene.String()
    spreadingCondition = SpreadingConditionInput()

class PassageInput(graphene.InputObjectType):
    pass_type = graphene.Int()
    max = graphene.Int()
    passes = graphene.List(PassInput)


class SimplePassageInput(graphene.InputObjectType):
    pass_type = graphene.Int()
    passage = graphene.Int()


class RequafertiComputeNInput(graphene.InputObjectType):
    ech_num_ana = graphene.String(required=True)
    ech_cp = graphene.Int()
    ech_geoid = graphene.List(graphene.Float)
    ech_x = graphene.Float()
    ech_y = graphene.Float()
    prairie_region = graphene.Int()
    prairie_type = graphene.Int()
    taux_humus = graphene.Int()
    taux_trefle = graphene.Int()
    herbe_pature = graphene.Float()
    mode_exploit_pature = graphene.Int()
    ugb_parcelle = graphene.Float()
    jours_pature = graphene.Float()
    ingestion_cat_anim = graphene.Int()
    ingestion_herbe = graphene.Float()
    herbe_ensile = graphene.Float()
    mode_exploit_ensile = graphene.Int()
    herbe_fauche = graphene.Int()
    mode_exploit_fauche = graphene.Int()
    freq_epand = graphene.Int()
    passages = graphene.List(PassageInput)
 


class RequafertiComputeNParamsInput(PydanticInputObjectType):
    class Meta:
        model = RequafertiComputeNParams
        name = "requaferti_grassland__compute_n_params"
       
    passages = graphene.List(PassageInput)  # Include passages field
    type_passages = graphene.List(SimplePassageInput)
    



class RequafertiNResultErrorType(PydanticObjectType):
    class Meta:
        model = RequafertiNResultError
        name = "RequafertiNResultError"

class RequafertiNResultEstimation(PydanticObjectType):
    class Meta:
        model = RequafertiNResultEstimation
        name =  "RequafertiNResultEstimation"


###





class RequafertiSoilResponseType(PydanticObjectType):
    class Meta:
        model = RequafertiGetSolResultType
        name = "requaferti_grassland__soil_response"


class RequafertiComputeNResultType(PydanticObjectType):
    class Meta:
        model = RequafertiComputeNResult
        name = "requaferti_grassland__compute_n_result"


class Query(graphene.ObjectType):

    requaferti_grassland__healthz = graphene.String(default_value="ok")
    requaferti_grassland__config = GenericScalar(
        description="Requaferti configuration, as received from Requasud"
    )  # a dict
    requaferti_grassland__postal_code = graphene.Field(
        graphene.String,
        description="Postal code",
        geometry=graphene.Argument(
            GenericScalar,
            required=True,
            description="__geo_interface__ dict, on EPSG:4258",
        ),
    )
    requaferti_grassland__agricultural_region = graphene.Field(
        graphene.String,
        description="Agricultural region",
        geometry=graphene.Argument(
            GenericScalar,
            required=True,
            description="__geo_interface__ dict, on EPSG:4258",
        ),
    )
    requaferti_grassland__soil = graphene.Field(
        RequafertiSoilResponseType,
        description="Soil properties",
        geometry=graphene.Argument(
            GenericScalar,
            required=True,
            description="__geo_interface__ dict, on EPSG:4258",
        ),
        couche_arable=graphene.Argument(graphene.Float, required=False),
        argile=graphene.Argument(graphene.Float, required=False),
        calcaire=graphene.Argument(graphene.Float, required=False),
        carbone=graphene.Argument(graphene.Float, required=False),
        nnh4_profil=graphene.Argument(graphene.Float, required=False),
        nno3_profil=graphene.Argument(graphene.Float, required=False),
        densite=graphene.Argument(graphene.Float, required=False),
        cailloux=graphene.Argument(graphene.Float, required=False),
        rapport_c_n=graphene.Argument(graphene.Float, required=False),
        mo=graphene.Argument(graphene.Float, required=False),
        nhumus=graphene.Argument(graphene.Float, required=False),
    )
   
   
    requaferti_grassland__compute_n = graphene.Field(
        RequafertiComputeNResultType,
        description="Compute nitrogen recommendation",
        params=graphene.Argument(RequafertiComputeNParamsInput, required=True),
    )

    requaferti_grassland__generate_results_pdf = graphene.NonNull(
        PDFGeneratorOutputType,
        description="Generate the pdf file for the Requaferti results",
        input=graphene.Argument(PDFGeneratorInputType, required=True),
    )
 
    requaferti_grassland__NS_SM = GenericScalar(
        geometry=graphene.Argument(
            GenericScalar,
            required=True,
            description="__geo_interface__ dict, on EPSG:4258",
        ),
    )

    requaferti_grassland__treffle = graphene.List(
        PrairieFournitureLeguType,
        pasture_type = graphene.String()
    )

    requaferti_grassland__ugb = GenericScalar(
         description="UGB list"
    )
        
    requaferti_grassland__exploitation_mode = GenericScalar(
        description = "Exploitation mode combinaison"
    )

    requaferti_grassland__organic_matter_passes = GenericScalar(
        description= "Organic matter passess"
    )

    requaferti_grassland__fertilizer_passes = GenericScalar(
        description= "Fertilizer passess"
    )

    requaferti_grassland__clover_coverage_images = graphene.List(
        CloverCoveragePictures,
        pasture_type = graphene.String()

    )
    
    async def resolve_requaferti_grassland__NS_SM(self, info, geometry):
        try:
            geometry = Projection.etrs89_to_belgian_lambert_72(
                    shapely.geometry.shape(geometry)
                ).__geo_interface__

            result = await requaferti_client.get_requasud_code_postal(geometry=geometry)
            return result
        except Exception as exc:
                logger.exception("Failed to compute region")
                raise GraphQLServiceError() from exc


    def resolve_requaferti_grassland__config(self, info):
        try:
            return requaferti_client.get_config()
        except Exception as exc:
            logger.exception("Failed to get the configuration!")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_grassland__postal_code(self, info, geometry):
        try:
            geometry = Projection.etrs89_to_belgian_lambert_72(
                shapely.geometry.shape(geometry)
            ).__geo_interface__

            code_postal = requaferti_client.get_code_postal(geometry)
            return code_postal
        except Exception as exc:
            logger.exception("Failed to compute code postal")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_grassland__agricultural_region(self, info, geometry):
        try:
            geometry = Projection.etrs89_to_belgian_lambert_72(
                shapely.geometry.shape(geometry)
            ).__geo_interface__
            region_agricole = requaferti_client.get_region_agricole(geometry)
            return region_agricole
        except Exception as exc:
            logger.exception("Failed to compute region agricole")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_grassland__soil(self, info, geometry, **kwargs):
        try:
            geometry = Projection.etrs89_to_belgian_lambert_72(
                shapely.geometry.shape(geometry)
            ).__geo_interface__

            sol = requaferti_client.get_sol(geometry, **kwargs)
            return sol
        except Exception as exc:
            logger.exception("Failed to get sol")
            raise GraphQLServiceError() from exc

    async def resolve_requaferti_grassland__compute_n(self, info, params):
        try:
            params_dict = RequafertiComputeNParams(**params)
            result = await requaferti_client.compute_n(params=params_dict)

        except Exception as exc:
            logger.exception("Failed to compute nitrogen")
            raise GraphQLServiceError() from exc

        return result

    def resolve_requaferti_grassland__generate_results_pdf(self, info, input):

        inputs = input["inputs"]

        try:
            # Get algorithm logo as static image
            with open(config.APP_DIR / "static/logo_requaferti.png", "rb") as f:
                logo = f.read()
                inputs["algorithm_logo_base64"] = (
                    "data:image/png;base64," + base64.b64encode(logo).decode()
                )
        except Exception as exc:
            logger.exception("Failed to read algorithm logo")
            raise GraphQLServiceError() from exc

        try:
            language = info.context["request"].headers["x-hasura-language"]

        except:
            logger.exception("Could not retrieve language from info, will use default template")
            language = 'fr'

        with tracer().start_as_current_span("generate_pdf"):
            try:
                template_file = "base_de.html" if language == 'de' else 'base.html'
                template_path =  "templates/pdf/" + template_file
                with open(config.APP_DIR / template_path, "r") as f:
                    template = f.read()
                response = requests.post(
                    config.PDF_GENERATOR_ENDPOINT,
                    headers={},
                    json={"template": template, "data": inputs},
                )
                response.raise_for_status()

                return PDFGeneratorOutputType(pdf=response.content)

            except Exception as exc:
                logger.exception("Failed to build pdf")
                raise GraphQLServiceError() from exc

    requaferti_grassland__grassland__dico_list = GenericScalar(
        description="Requaferti grassland list configuration"
    )  # a dict
    #
    def resolve_requaferti_grassland__grassland__dico_list(self, info):
        try:
            return requaferti_client.get_list()
        except Exception as exc:
            logger.exception("Failed to get the configuration!")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_grassland__treffle(self,info, pasture_type=None):
        try:
            return requaferti_client.get_leguminous_contribution(pasture_type)   
        except Exception as exc:
            logger.exception("Failed to get the leguminous rating !")
            raise GraphQLServiceError() from exc
        
    def resolve_requaferti_grassland__ugb(self,info):
        try:
            return ugb_client.bovin().ovin().get_list_ugb()
        except Exception as exc:
            logger.exception("Failed to get the ugb !")
            raise GraphQLServiceError() from exc

    async def resolve_requaferti_grassland__exploitation_mode(self,info):
        try:
           return requaferti_client.get_exploitation_mode()
        except Exception as exc:
            logger.exception("Failed to get the ugb !")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_grassland__organic_matter_passes(self,info):
        try:
            return [{
        "pass_type": 1,
        "max": 2,
        "passes": []
      }, {
        "pass_type": 2,
        "max": 1,
        "passes": []
      },
      {
        "pass_type": 3,
        "max": 1,
        "passes": []
      },
      {
        "pass_type": 4,
        "max": 1,
        "passes": []
      }]
        except Exception as exc:
            logger.exception("Failed to get the organic matter passes !")
            raise GraphQLServiceError() from exc
    

    def resolve_requaferti_grassland__fertilizer_passes(self,info):
        try:
            return [{
        "pass_type": 1,
        "passage": None
      }, {
        "pass_type": 2,
        "passage": None
      },
      {
        "pass_type": 3,
        "passage": None
      },
      {
        "pass_type": 4,
        "passage": None
      },
      {
        "pass_type": 5,
        "passage": None 
      }]
        except Exception as exc:
            logger.exception("Failed to get the organic matter passes !")
            raise GraphQLServiceError() from exc
        

    def resolve_requaferti_grassland__clover_coverage_images(self,info, pasture_type=None):
        ## Return pictures based on the pasture_type
        folder_path = os.path.join(config.REQUAFERTI_RESOURCES_IMAGES_PATH,"clover_coverage/")  # Replace with your folder path
        images = []

        if pasture_type == "1":
            folder_path = os.path.join(folder_path,"p_temp/")

        if pasture_type == "2":
            folder_path = os.path.join(folder_path,"p_perm/")

        for filename in os.listdir(folder_path):
            if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.gif', '.bmp')):  # Check for image files
                file_path = os.path.join(folder_path, filename)
                base64_image = encode_image_to_base64(file_path)
                images.append({
                    'url': f'data:image/jpeg;base64,{base64_image}',
                    'name': filename
                })
        return sorted(images, key=lambda x: get_numeric_part(x['name']))
    
##global function
def encode_image_to_base64(image_path):
    with open(image_path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode("utf-8")
    
def get_numeric_part(filename):
    # Extract the numeric part of the filename (assuming the format 'prc_<number>.png')
    match = re.search(r'\d+', filename)
    return int(match.group()) if match else 0