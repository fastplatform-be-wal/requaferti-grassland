from datetime import date
from typing import Any, Dict, List, Optional, Union

import logging
import graphene

from pydantic import ValidationError, root_validator
from pydantic import BaseModel, Field


logger = logging.getLogger(__name__)


def check_all_or_none(values, fields):
    values_has_fields = [f is not None in values for f in fields]
    if not all(values_has_fields) and any(values_has_fields):
        raise ValidationError(
            f"All the following fields should be filled (or none): {fields}"
        )
    


class AnimalCategory(BaseModel):
    id: str
    libelle: Optional[str]
    libelle_de: Optional[str]

class OrganicMatterType(BaseModel):
    id: str
    type_eff_xid: str
    type_eff_code: Optional[str]
    type_eff_libelle: str
    type_eff_libelle_de: str
    grp_ani_xid: str
    grp_ani_libelle: str
    grp_ani_libelle_de: str
    azote_total: str

class SpreadingCondition(BaseModel):
    id: str
    libelle: Optional[str]
    libelle_de: Optional[str]

class Pass(BaseModel):
    id: str
    animalCategory: AnimalCategory
    organicMatterType: OrganicMatterType
    effluentQuantity: int
    nitrogenContent: float
    applicationDate: Optional[str]
    spreadingCondition: SpreadingCondition

class Passage(BaseModel):
    pass_type: int
    max: int
    passes: List[Pass]
    
    @root_validator
    @classmethod
    def check_pass_type(cls, values):
        if values["pass_type"] is None :
            raise ValidationError("pass_type must be provided")
        return values
    
class PassageSimple(BaseModel):
    pass_type: int
    passage: Optional[int]


class RequafertiComputeNParams(BaseModel):

    # info de l'echantillon
    ech_num_ana: str  # numéro analyse / example: "test ws"
    ech_cp: Optional[int]  # Code postal / example: 5030
    ech_geoid: Optional[str]  # Spécifique pour certains clients
    ech_x: Optional[float]  # Easting (X)
    ech_y: Optional[float]  # Northing (Y)
    superficie: float

    prairie_region: int
    prairie_type: int

    taux_humus: Optional[int]

    taux_trefle:int

    herbe_pature: Optional[float]
    mode_exploit_pature: Optional[int]
    ugb_parcelle: Optional[float]
    jours_pature: Optional[float]
    ingestion_cat_anim: Optional[int]
    ingestion_herbe: Optional[float]

    herbe_ensile: Optional[float]
    mode_exploit_ensile: Optional[int]
    
    herbe_fauche: Optional[int]
    mode_exploit_fauche: Optional[int]


    # apport organique

    freq_epand: Optional[int]  # Fréquence / example: 3

    passages: Optional[List[Passage]]
    type_passages: Optional[List[PassageSimple]]


    @root_validator
    @classmethod
    def check_ech(cls, values):
        if values["ech_cp"] is None and (
            values["ech_x"] is None or values["ech_y"] is None
        ):
            raise ValidationError("ech_cp or ech_x and ech_y must be provided")
        return values
    

    @classmethod
    def to_input(cls, instance):
        # Convert Pydantic instance to a dictionary
        return instance.dict()

    @classmethod
    def from_input(cls, input):
        # Convert input dictionary to Pydantic instance
        return RequafertiComputeNParams(**input)

    

class RequafertiNResultError(BaseModel):
    r_fourn_sol_erreur:Optional[str]
    r_apport_total_n_erreur: Optional[str]
    r_complement_n_erreur: Optional[str]


class RequafertiNResultEstimation(BaseModel):
    id:str
    r_dose: Optional[int]
    r_conseil_theorique: Optional[int]
    r_complement_n_erreur: Optional[str]

    @root_validator
    def debug_validation(cls, values):
        return values

    def dict(self, **kwargs):
        return super().dict(exclude_unset=True, **kwargs)
   


class RequafertiComputeNResult(BaseModel):
    r_besoin_total_n: Optional[float]  # Besoins totaux en azote (uN) / exaample: 280
    r_fourniture_legu: Optional[float]  # Fournitures en azote (uN) / example: 225
    r_rendement_total: Optional[float]
    r_fourn_sol:Optional[str]
    r_restitution_pature:Optional[float]
    r_fourn_orga_total:Optional[float]
    r_apport_total_n:Optional[float]
    r_complement_n:Optional[float]
    r_estimation:Optional[List[RequafertiNResultEstimation]]
    error: Optional[RequafertiNResultError]



class RequafertiGetSolResultType(BaseModel):
    nno3_profil: Optional[float] = Field(description='Profil NNO3 (kg N/ha)')
    nnh4_profil: Optional[float] = Field(description='Profil NNH4 (kg N/ha)')
    cailloux: Optional[float] = Field(description='Charge caillouteuse (%)')
    densite: Optional[float] = Field(description='Densité')
    couche_arable: Optional[float] = Field(description='Profondeur de la couche arable (cm)')
    temp_air: Optional[float] = Field(description="Température de l'air moyenne (°C)")
    argile: Optional[float] = Field(description='Teneur en argile (%)')
    calcaire: Optional[float] = Field(description='Teneur en calcaire (‰)')
    mo: Optional[float] = Field(description='Teneur en matière organique (%)')
    carbone: Optional[float] = Field(description='Teneur en carbone (%)')
    km: Optional[float] = Field(description='Coefficient de minéralisation (Km)')
    fact_conv: Optional[float] = Field(description='Facteur de conversion')
    nhumus: Optional[float] = Field(description='N humus (g/kg ou ‰)')
    reliquat_n: Optional[float] = Field(description='Reliquat (kg/ha)')
    pot_mineral: Optional[float] = Field(description='Potentiel de minéralisation')

    rapport_c_n: Optional[float] = Field(description='Rapport carbone/azote')


class PDFGeneratorInputType(graphene.InputObjectType):
    inputs = graphene.JSONString(required=True)

    class Meta:
        name = "requaferti__pdf_generator_input"


class PDFGeneratorOutputType(graphene.ObjectType):
    pdf = graphene.Base64()

    class Meta:
        name = "requaferti__pdf_generator_output"


class PrairieFournitureLeguType(graphene.ObjectType):
    id = graphene.String()
    prairie_type_id = graphene.String()
    prairie_type_libelle = graphene.String()
    prairie_type_libelle_de = graphene.String()
    prairie_taux_trefle_id = graphene.String()
    taux_trefle_libelle = graphene.String()

class CloverCoveragePictures(graphene.ObjectType):
    name = graphene.String()
    url = graphene.String()