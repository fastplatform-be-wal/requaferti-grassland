class UniteGrosBetail:

    def __init__(self):
        self.list_ugb = {}

    def bovin(self):
        ugb_bovin = [
            {"libelle": "vache allaitante + veau","libelle_de": "Spanferkel + Kalb", "ugb":1},
            {"libelle": "vache laitière", "libelle_de": "Milchkuh","ugb":1},
            {"libelle": "génisse > 2 ans", "libelle_de": "Färse > 2 Jahre","ugb":0.75},
            {"libelle": "génisse 1 à 2 ans","libelle_de": "Färse 1 bis 2 Jahre alt", "ugb":0.6},
            {"libelle": "génisse < 1 an","libelle_de": "Färse < 1 Jahr", "ugb":0.4},
            {"libelle": "taureau", "libelle_de": "Stier","ugb":1}
        ]

        self.list_ugb["bovin"] = ugb_bovin
        return self

    def ovin(self):
        ugb_ovin = [
            {"libelle": "brebis","libelle_de": "Schaf", "ugb":0.15},
            {"libelle": "agnelle","libelle_de": "Lamm", "ugb":0.15},
            {"libelle": "bélier","libelle_de": "Ram", "ugb":0.15},
            {"libelle": "agneau d'herbe","libelle_de": "Graslamm", "ugb":0.05},
            {"libelle": "agneau de bergerie","libelle_de": "Schafstalllamm", "ugb":0}
        ]

        self.list_ugb["ovin"] = ugb_ovin
        return self

    def get_list_ugb(self):
        return self.list_ugb


ugb_client = UniteGrosBetail()