This postal districts layer was downloaded on 2021-10-12 at [https://www.geo.be/catalog/details/9738c7c0-5255-11ea-8895-34e12d0f0423?l=fr]().

The following terms of use apply:

> **Conditions d'utilisation**
> 
> *Restrictions concernant l’accès public*
>
> - Le gestionnaire du jeu de données tel qu’il est défini plus haut possède les droits de propriété (y compris les droits de propriété intellectuelle) se rapportant aux fichiers.
> - Le gestionnaire accorde au client le droit d’utiliser les données pour son usage interne.
> - L’usage des données à des fins commerciales, sous quelque forme que ce soit, est formellement interdit.
> - Le nom du gestionnaire doit apparaître lors de chaque utilisation publique des données.
>
> *Conditions applicables à l’accès et à l’utilisation*
>
> Pas de restrictions concernant l'accès public