import logging
import asyncio
from datetime import datetime
import csv

import httpx
import cachetools
from threading import Lock
import fiona
from shapely.geometry import shape
from shapely.strtree import STRtree


from app.settings import config
from app.lib.requaferti_types import (
    RequafertiComputeNParams,
    RequafertiComputeNResult,
    RequafertiGetSolResultType,
)


logger = logging.getLogger(__name__)


class RequafertiClient:
    def __init__(self):
        self.http_client = None

        self.codes_postaux_tree = None
        self.codes_postaux_index = None
        self.regions_agricoles_tree = None
        self.regions_agricoles_index = None
        self.charge_caillouteuse_tree = None
        self.charge_caillouteuse_index = None
        self.parametres_sol_par_code_postal = None
        self.parametres_sol_par_region_agricole = None
        self.parametres_sol_par_defaut = None

        self.configuration_lock = asyncio.Lock()
        self.configuration = None
        self.configuration_updated_at = None

        self.compute_n_cache = cachetools.TTLCache(
            maxsize=config.REQUAFERTI_COMPUTE_N_CACHE_MAX_SIZE,
            ttl=config.REQUAFERTI_COMPUTE_N_CACHE_TTL,
        )
        self.compute_n_cache_lock = Lock()

    def create_http_client(self):
        self.http_client = httpx.AsyncClient()

    async def close_http_client(self):
        await self.http_client.aclose()

    def load_geographic_data(self):
        """Load and index the GIS layers from shapefiles

        - codes postaux
        - régions agricoles

        """
        logger.info("Loading geomgraphic data in memory")

        logger.info("Loading codes postaux")
        with fiona.open(
            config.APP_DIR / "lib/codes_postaux/codes_postaux.shp", "r"
        ) as data:
            shapes = [shape(f["geometry"]) for f in data]
            self.codes_postaux_tree = STRtree(shapes)
            self.codes_postaux_index = dict((id(s), f) for s, f in zip(shapes, data))

        logger.info("Loading régions agricoles")
        with fiona.open(
            config.APP_DIR / "lib/regions_agricoles/regions_agricoles.shp", "r"
        ) as data:
            shapes = [shape(f["geometry"]) for f in data]
            self.regions_agricoles_tree = STRtree(shapes)
            self.regions_agricoles_index = dict(
                (id(s), f) for s, f in zip(shapes, data)
            )

        logger.info("Loading charge caillouteuse")
        with fiona.open(
            config.APP_DIR / "lib/sol/charge_caillouteuse/charge_caillouteuse.shp", "r"
        ) as data:
            shapes = [shape(f["geometry"]) for f in data]
            self.charge_caillouteuse_tree = STRtree(shapes)
            self.charge_caillouteuse_index = dict(
                (id(s), f) for s, f in zip(shapes, data)
            )

        logger.info("Loading parametres sol par code postal")
        with open(
            config.APP_DIR / "lib/sol/parametres_sol_par_code_postal.csv"
        ) as csvfile:
            reader = csv.DictReader(csvfile)
            self.parametres_sol_par_code_postal = {}
            for row in reader:
                code_postal = row.pop("code_postal")
                self.parametres_sol_par_code_postal[str(code_postal)] = {
                    k: float(v) if v != "" else None for k, v in row.items()
                }

        logger.info("Loading parametres sol par région agricole")
        with open(
            config.APP_DIR / "lib/sol/parametres_sol_par_region_agricole.csv"
        ) as csvfile:
            reader = csv.DictReader(csvfile)
            self.parametres_sol_par_region_agricole = {}
            for row in reader:
                region_agricole = row.pop("region")
                self.parametres_sol_par_region_agricole[region_agricole] = {
                    k: float(v) if v != "" else None for k, v in row.items()
                }

        logger.info("Loading parametres sol par défaut")
        with open(config.APP_DIR / "lib/sol/parametres_sol_par_defaut.csv") as csvfile:
            reader = csv.DictReader(csvfile)
            row = next(reader)
            self.parametres_sol_par_defaut = {
                k: float(v) if v != "" else None for k, v in row.items()
            }

    async def get_config(self):
        """Loads the Requaferti configuration from the `liste` and `dico` Requasud web
        services

        The config is cached in memory for a duration of REQUAFERTI_CONFIG_CACHE_TTL
        seconds
        """
        logger.debug(f"self.configuration is None: {self.configuration is None}")
        logger.debug(f"self.configuration_updated_at is None: {self.configuration_updated_at is None}")

        if self.configuration is not None and (
            (datetime.now() - self.configuration_updated_at).total_seconds() <= config.REQUAFERTI_CONFIG_CACHE_TTL
        ):
            logger.debug("Reading config from cache")
            return self.configuration

        logger.debug("Refreshing config from the Requaferti endpoint")
        # Lock the configuration for updating
        async with self.configuration_lock:
            try:
                logger.info(f"Retrieving config for dico list {config.REQUAFERTI_CONFIG_LISTE_DICOS}")
                if(config.REQUAFERTI_CONFIG_LISTE_DICOS !="None"):
                    liste_dicos = config.REQUAFERTI_CONFIG_LISTE_DICOS.split(",")
                else:
                    dicoResponse = await self.http_client.post(
                                        config.REQUAFERTI_CONFIG_DICO_LIST_URL,
                                        timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
                                        auth=(config.REQUAFERTI_USERNAME, config.REQUAFERTI_PASSWORD,),
                                        )
                    dicoArray = dicoResponse.json()["liste_dico"]
                    liste_dicos = [dico["dico_flag"] for dico in dicoArray]  
                

                # This semaphore will block the number of downloads under the max
                # concurrency
                semaphore = asyncio.Semaphore(config.REQUAFERTI_CONFIG_DICO_CONCURRENCY)
                configuration = {}
                async def download_dico(dico):
                    logger.debug("Refreshing config from the Requaferti endpoint: %s", dico)
                    async with semaphore:
                        response = await self.http_client.post(
                            config.REQUAFERTI_CONFIG_DICO_URL,
                            data={"dico_flag": dico},
                            timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
                            auth=(
                                config.REQUAFERTI_USERNAME,
                                config.REQUAFERTI_PASSWORD,
                            ),
                        )
                        response.raise_for_status()
                        data = response.json()["dico"]
                        # Write to the config dict, under a lock to prevent concurrent
                        # writes
                        async with asyncio.Lock():
                            configuration[dico] = data

                    logger.debug(f"Completed download for dico: {dico}")

                download_dicos = [download_dico(dico) for dico in liste_dicos]
                await asyncio.gather(*download_dicos)
            except Exception as e:
                logger.error(f"Failed to refresh the configuration: {str(e)}")
                raise
            else:
                self.configuration = configuration
                self.configuration_updated_at = datetime.now()

        return self.configuration

    def get_code_postal(self, geometry):
        """Return the postal code of a geometry

        Args:
            geometry (dict): a __geo_interface__ dict on EPSG:31370

        This is computed on the basis of the codes_postaux/codes_postaux.shp
        file in the lib directory.

        Returns:
            str: the postal code
        """
        geometry = shape(geometry)
        code_postal_feature = self._get_max_intersection_from_geometry_RG(
            geometry, self.codes_postaux_index
        )
        return (
            code_postal_feature["properties"]["nouveau_PO"]
            if code_postal_feature is not None
            else None
        )

    def get_region_agricole(self, geometry):
        """Return the agricultural region of a geometry

        This is computed on the basis of the regions_agricoles/regions_agricoles.shp
        file in the lib directory.

        Args:
            geometry (dict): a __geo_interface__ dict on EPSG:31370

        Returns:
            str: the name of the agricultural region
        """
        geometry = shape(geometry)
        region_agricole_feature = self._get_max_intersection_from_geometry_RG(
            geometry, self.regions_agricoles_index
        )
        return (
            region_agricole_feature["properties"]["NOM"]
            if region_agricole_feature is not None
            else None
        )

    def get_charge_caillouteuse(self, geometry):
        geometry = shape(geometry)
        return self._get_avg_field_intersection_from_geometry(
            geometry,
            self.charge_caillouteuse_tree,
            self.charge_caillouteuse_index,
            "CH_EST_SUR",
        )

    def _get_max_intersection_from_geometry_RG(self, geometry, index):

        for shape_id, feature in index.items():
            polygon_geometry = shape(feature["geometry"])
            if polygon_geometry.intersects(geometry):
                # Get the properties for the shape ID
                logger.debug(f"Properties for shape ID {shape_id}: {feature['properties']}")

                # Return the feature corresponding to the shape ID
                return feature

        # If no intersection is found, return None
        return None

    def _get_max_intersection_from_geometry(self, geometry, tree, index):
        """Return the feature from a layer that max intersects the provided geometry

        Args:
            geometry (shape): a Shapely geometry object
            tree (STRTree): the index tree as built from the source layer
            index (dict): an index mapping the memory id and a feature, as built from
                            the source layer

        Returns:
            geometry (dict): a __geo_interface__ dict on EPSG:31370
        """
        # Memory IDs of shapes that intersect the geometry
        shape_ids = [id(s) for s in tree.query(geometry)]

        if not shape_ids:
            return None

        # Features corresponding to these memory IDs, as per the index
        features = [index[shape_id] for shape_id in shape_ids]

        # Find max intersection
        intersection_areas = [
            shape(f["geometry"]).intersection(geometry).area for f in features
        ]
        index_intersection_area_max = max(
            range(len(intersection_areas)), key=lambda i: intersection_areas[i]
        )

        return features[index_intersection_area_max]

    def _get_avg_field_intersection_from_geometry(self, geometry, tree, index, field):
        """Return the average value of a field of all the features from a layer that 
        intersect the provided geometry

        Args:
            geometry (shape): a Shapely geometry object
            tree (STRTree): the index tree as built from the source layer
            index (dict): an index mapping the memory id and a feature, as built from
                            the source layer
            field (str): which field to use for the average

        Returns:
            geometry (dict): a __geo_interface__ dict on EPSG:31370
        """
        # Memory IDs of shapes that intersect the geometry
        shape_ids = [id(s) for s in tree.query(geometry)]

        if not shape_ids:
            return None

        # Features corresponding to these memory IDs, as per the index
        features = [index[shape_id] for shape_id in shape_ids]

        # Find weighted averageof the field on the intersecting geometries
        intersection_areas = [
            shape(f["geometry"]).intersection(geometry).area for f in features
        ]
        values = [float(f["properties"].get(field)) for f in features]

        # If all the intersecting geometries have no value, return None
        if all([v is None for v in values]):
            return None

        # If we are querying with a geometry that is a Point, then the intersection
        # areas will be zero for all intersections. In that case, we will just be
        # returning the average of the values.
        if all([i is None or i == 0 for i in intersection_areas]):
            intersection_areas = [1] * len(intersection_areas)

        # Return the average of the values of the field, weighted by the intersection
        # areas
        average = sum(
            [i * v for i, v in zip(intersection_areas, values) if v is not None]
        ) / sum([i for i, v in zip(intersection_areas, values) if v is not None])

        return average

    def get_sol_par_code_postal(self, code_postal):
        return self.parametres_sol_par_code_postal.get(str(code_postal), None)

    def get_sol_par_region_agricole(self, region_agricole):
        return self.parametres_sol_par_region_agricole.get(region_agricole, None)

    def get_sol_par_defaut(self):
        return self.parametres_sol_par_defaut

    def get_sol(self, geometry, **kwargs):

        sol = RequafertiGetSolResultType()

        # First assign default values for whole Wallonia
        for k, v in requaferti_client.get_sol_par_defaut().items():
            if v is not None:
                setattr(sol, k, v)

        # Then override with values from the region agricole
        region_agricole = requaferti_client.get_region_agricole(geometry)
        if region_agricole is not None:
            sol_region_agricole = requaferti_client.get_sol_par_region_agricole(
                region_agricole
            )
            if sol_region_agricole is not None:
                for k, v in sol_region_agricole.items():
                    if v is not None:
                        setattr(sol, k, v)

        # Then override with values from the postal code
        code_postal = requaferti_client.get_code_postal(geometry)
        if code_postal is not None:
            sol_code_postal = requaferti_client.get_sol_par_code_postal(code_postal)
            if sol_code_postal is not None:
                for k, v in sol_code_postal.items():
                    if v is not None:
                        setattr(sol, k, v)

        # Then override with values from the charge_caillouteuse layers
        cailloux = requaferti_client.get_charge_caillouteuse(geometry)
        if cailloux is not None:
            cailloux = round(cailloux, 2)
            setattr(sol, "cailloux", cailloux)

        # Then override with values from the user (passed as arguments)
        for k, v in kwargs.items():
            if v is not None:
                setattr(sol, k, v)

        # Compute automatic coefficients and convert units as necessary
        # for Requaferti
        if sol.couche_arable is not None:
            sol.couche_arable *= 100  # m to cm

        if sol.calcaire is not None:
            sol.calcaire *= 10  # percent to permille

        if sol.temp_air is not None:
            sol.fact_conv = max(0.2 * sol.temp_air - 1, 0)

        if sol.carbone is not None and sol.mo is None:
            sol.mo = 2 * sol.carbone

        if (
            sol.mo is not None
            and sol.fact_conv is not None
            and sol.rapport_c_n is not None
            and sol.rapport_c_n != 0
            and sol.fact_conv != 0
            and sol.nhumus is not None
        ):
            sol.nhumus = sol.mo / (sol.rapport_c_n * sol.fact_conv) * 10

        if (
            sol.fact_conv is not None
            and sol.argile is not None
            and sol.calcaire is not None
        ):
            sol.km = (
                1200 * sol.fact_conv / ((sol.argile + 200) * (sol.calcaire * 0.3 + 200))
            )

        return sol
    

    def _process_parameters(self,query_params):
        """
        Process parameters from the input query_params and update them accordingly.

        Parameters:
        - query_params (dict): The input parameters dictionary.

        Returns:
        None

        This function processes passages in the query_params dictionary, extracts relevant information, 
        and updates the dictionary with the processed results.

        It specifically looks for passages related to certain pass_types (1 to 4) and organizes the information 
        into a structured result dictionary. The keys in the result dictionary follow a specific pattern 
        based on the pass_type, letter representation, and the type of information (e.g., cat_anim_passage_1a).

        The processed result is then added to the original query_params dictionary to ensure it includes 
        all the relevant information.

        Note: This function modifies the input query_params dictionary in place.

        Example:
        _process_parameters({
            "passages": [...], 
            "other_key": "other_value"
        })

        After processing, the query_params dictionary will be updated with additional key-value pairs 
        based on the processed information from passages.

        """
        passArray = query_params.pop("passages")
        if passArray is None:
            return

        result = {}  # Dictionary to store the final result
        passages = {i: None for i in range(1, 5)}  # Initialize a dictionary to store passages

        # Iterate over passages and store them in the dictionary
        for passage in passArray:
            pass_type = passage['pass_type']
            if 1 <= pass_type <= 4:
                passages[pass_type] = passage

        # Process each passage
        for i in range(1, 5):
            passage = passages[i]
            if passage:
                passes = passage.get('passes', [])
                if passes:
                    max_passage = passage['max']
                    for j, pass_data in enumerate(passes):
                        letter=''
                        if max_passage >1:
                            letter = chr(97+j)
                        result[f'cat_anim_passage_{i}{letter}'] = pass_data.get('animalCategory', {}).get('id')
                        result[f'type_mo_passage_{i}{letter}'] = pass_data.get('organicMatterType', {}).get('type_eff_xid')
                        result[f'quantite_eff_passage_{i}{letter}'] = pass_data.get('effluentQuantity')
                        result[f'teneur_n_passage_{i}{letter}'] = pass_data.get('nitrogenContent')
                        result[f'cond_epand_passage_{i}{letter}'] = pass_data.get('spreadingCondition', {}).get('id')

        # Add result query to query_params
        for key, value in result.items():
            query_params[key] = value



    def _process_parameters_pass_type(self,query_params):
        passArray = query_params.pop("type_passages")

        if passArray is None:
            return

        result = {}  # Dictionary to store the final result
        passages = {i: None for i in range(1, 6)}  # Initialize a dictionary to store passages

        # Iterate over passages and store them in the dictionary
        for passage in passArray:
            pass_type = passage['pass_type']
            if 1 <= pass_type <= 5:
                passages[pass_type] = passage

        # Process each passage
        for i in range(1, 5):
            passage = passages[i]
            if passage:
                result[f'type_passage_{i}'] = passage.get('passage', 0)
                result[f'engrais_min_{i}'] =0
                result[f'teneur_n_min_{i}'] = 0
                result[f'r_dose_{i}'] = 0
                    

        # Add result query to query_params
        for key, value in result.items():
            query_params[key] = value

    async def compute_n(self, params: RequafertiComputeNParams):
        """Computes the N recommendation from Requaferti by caling the Requasud
        webservice

        A max of REQUAFERTI_COMPUTE_N_CACHE_MAX_SIZE results are cached in an TTLCache,
        and returned from there if existing.
        The results in the cache are expired after REQUAFERTI_COMPUTE_N_CACHE_TTL seconds.

        Args:
            params (RequafertiComputeNParams): Input parameters

        Raises:
            GraphQLError: [description]

        Returns:
            RequafertiComputeNResult: Recommendation result
        """

        # Read and return from cachUniteGrosBetaile if entry exists
        with self.compute_n_cache_lock:
            result = self.compute_n_cache.get(str(params))

        if result is not None:
            logger.debug("compute_n: reading result from cache")
            return result

        # Otherwise, query from the API
        query_params = params.dict()

        # Destructuration of Passage array
        self._process_parameters(query_params)
        self._process_parameters_pass_type(query_params)

        # If there is no value for herbe_pature  ugb_parcelle and jours_pature should be initialized

        if query_params['jours_pature'] is None and query_params['ugb_parcelle'] is None and query_params['herbe_pature'] is None:
            query_params['herbe_pature'] = 0
            # Those params cannot be null so tricked the system since if  herbe_pature is defined, the value of herbe_pature
            # is used primarily
            query_params['jours_pature'] = 1
            query_params['ugb_parcelle'] = 1

        elif query_params['herbe_pature'] is not None:
            query_params['ugb_parcelle'] = None
            query_params['jours_pature'] = None

        if query_params['superficie'] is not None:
            query_params['superficie'] = round(query_params['superficie']/10000,2)



        # Set default value if not set

        if query_params['mode_exploit_pature'] is None:
            query_params['mode_exploit_pature'] = 1

        if query_params['ingestion_cat_anim'] is None:
            query_params['ingestion_cat_anim'] = 38

        if query_params['ingestion_herbe'] is None:
            query_params['ingestion_herbe'] = 17.2

        if query_params['mode_exploit_ensile'] is None:
            query_params['mode_exploit_ensile'] = 3

        if query_params['herbe_ensile'] is None:
            query_params['herbe_ensile'] = 0

        if query_params['herbe_fauche'] is None:
            query_params['herbe_fauche'] = 0

        if query_params['mode_exploit_fauche'] is None:
            query_params['mode_exploit_fauche'] = 5

        # Convert Boolean params to 0 or 1 expected by the Requaferti API
        for param_name in query_params:
            if isinstance(query_params[param_name], bool):
                query_params[param_name] = 1 if query_params[param_name] else 0

        response = await self.http_client.post(
            config.REQUAFERTI_COMPUTE_N_URL,
            data=query_params,
            timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
            auth=(config.REQUAFERTI_USERNAME, config.REQUAFERTI_PASSWORD),
        )

        response_data = response.json()

        ### This code portion is used to solved the issue with apollo client
        ### The if no id is defined then the value are overwritten in the cache and the final result is wrong.
        if 'r_estimation' in response_data:
            i = 1
            for item in response_data['r_estimation']:
                item['id'] = 'dummy_' + str(i)
                i += 1
        ###

        response.raise_for_status()

        # Parse the API result
        result = RequafertiComputeNResult(**response_data)

        # Set the value in cache
        with self.compute_n_cache_lock:
            self.compute_n_cache[str(params)] = result
      
        # Return the result as a pydantic object
        return result

    def find_element_by_property(self,input_list, property_name, property_value):
        for element in input_list:
            if element.get(property_name) == property_value:
                return element
        return None


    async def get_list(self):
        try:
            response = await self.http_client.post(
                                config.REQUAFERTI_CONFIG_DICO_LIST_URL,
                                timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
                                auth=(
                                    config.REQUAFERTI_USERNAME,
                                    config.REQUAFERTI_PASSWORD,
                                ),
                            )
            response.raise_for_status()

            data = response.json()
            return data
        except Exception:
            logger.exception("Failed to retrieve the dico_list")
            raise

    async def get_requasud_code_postal(self,geometry):
        logger.debug("get_requasud_code_postal")
        try:
            config = await self.get_config()
            logger.debug(f"config retrieved")
            postal_code = self.get_code_postal(geometry=geometry)
            logger.debug(f"postal code retrieved: {postal_code}")
            return self.find_element_by_property(config["code_postaux"],"cp_code",postal_code)
        except Exception:
            logger.error("Failed to refresh the for postal code")
            raise

    async def get_leguminous_contribution(self,pasture_type=None):
        try:
            config = await self.get_config()
            contributionArray = config["prairie_fourniture_legu"]
            if pasture_type is None:
                return contributionArray
            
            return [c for c in contributionArray if c["prairie_type_id"] == pasture_type]
        except Exception:
            logger.error("Failed to refresh the configuration")
            raise


    async def get_exploitation_mode(self):
        try:
            dico = "prairie_type_mode_exploitation"
            response = await self.http_client.post(
                                config.REQUAFERTI_CONFIG_DICO_URL,
                                data={"dico_flag": dico},
                                timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
                                auth=(
                                    config.REQUAFERTI_USERNAME,
                                    config.REQUAFERTI_PASSWORD,
                                ),
                            )
            response.raise_for_status()

            data = response.json()["dico"]
            return data
        except Exception:
            logger.exception("Failed to refresh the configuration")
            raise


    
requaferti_client = RequafertiClient()
