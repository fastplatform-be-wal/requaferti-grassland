load("@io_bazel_rules_docker//python:image.bzl", "py_layer")
load("@io_bazel_rules_docker//python3:image.bzl", "py3_image")
load("@io_bazel_rules_docker//container:container.bzl", "container_image")
load("@fertilization_requaferti_grassland_pip//:requirements.bzl", "all_requirements")
load("@rules_pkg//:pkg.bzl", "pkg_tar")
load("@rules_python//python:defs.bzl", "py_binary")
load("//tools:constants.bzl", "PY37_IMAGE_BASE")
load("//tools/macros:uvicorn.bzl", "uvicorn_run_script")

package(default_visibility = ["//services/fertilization:__pkg__"])

filegroup(
    name = "srcs",
    srcs = glob(
        [
            "**/*.py",
        ],
        exclude = [
            ".venv/**/*.py",
            "**/__pycache__/*"
        ],
    ),
)

filegroup(
    name = "data",
    srcs = glob(
        [
            "app/lib/codes_postaux/**/*",
            "app/lib/regions_agricoles/**/*",
            "app/lib/sol/**/*",
            "app/lib/ugb/*",
            "app/static/**/*",
            "app/templates/**/*.html",
            "images/**/*"
        ]
    ),
)


py_binary(
    name = "requaferti_grassland",
    srcs = [
        ":srcs",
        ":requaferti_grassland_uvicorn_run",
    ],
    imports = ["."],
    main = "requaferti_grassland_uvicorn_run.py",
    deps = all_requirements,
)

uvicorn_run_script(
    name = "requaferti_grassland_uvicorn_run",
    app = "app.main:app",
    config = "app.settings:config",
)

container_image(
    name = "image",
    base = ":image_layers",
    env = {
        "PORT": "8000",
    },
    tars = [
        ":openssl_config",
    ],
)

py3_image(
    name = "image_layers",
    srcs = [
        ":srcs",
        ":requaferti_grassland_uvicorn_run",
    ],
    base = PY37_IMAGE_BASE,
    data = [
        ":data",
    ],
    layers = [
        ":layer_requirements",
    ],
    main = "requaferti_grassland_uvicorn_run.py",
)

py_layer(
    name = "layer_requirements",
    deps = all_requirements,
)

filegroup(
    name = "kustomize_resources",
    srcs = [
        "kustomization.yaml",
    ] + glob([
        "deploy/base/*",
    ]),
)

pkg_tar(
    name = "openssl_config",
    srcs = [
        "openssl.cnf",
    ],
    package_dir = "/usr/lib/ssl",
)
